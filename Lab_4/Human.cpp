#include "Human.h"
#include "service_functions.h"

Human::Human()
{
	Surname = SetStr(" ");
	Name = SetStr(" ");
	Patronymic = SetStr(" ");
}

Human::Human(const Human& human)
{
	Surname = SetStr(human.Surname);
	Name = SetStr(human.Name);
	Patronymic = SetStr(human.Patronymic);
}

Human::~Human()
{
	delete[] Surname;
	delete[] Name;
	delete[] Patronymic;
}

Human& Human::operator= (const Human& human)
{
	if (this == &human) return *this;

	delete[] this->Surname;
	this->Surname = SetStr(human.Surname);

	delete[] this->Name;
	this->Name = SetStr(human.Name);

	delete[] this->Patronymic;
	this->Patronymic = SetStr(human.Patronymic);

	return *this;
}

bool Human::operator== (const Human& RightHuman)
{
	bool result = (strcmp(this->Surname, RightHuman.Surname) == 0);
	result *= (strcmp(this->Name, RightHuman.Name) == 0);
	result *= (strcmp(this->Patronymic, RightHuman.Patronymic) == 0);
	return result;
}

std::istream& operator>> (std::istream& in, Human& human)
{
	std::cout << "������� �������: ";
	delete[] human.Surname;
	human.Surname = SetStr(in);

	std::cout << "������� ���: ";
	delete[] human.Name;
	human.Name = SetStr(in);

	std::cout << "������� ��������: ";
	delete[] human.Patronymic;
	human.Patronymic = SetStr(in);

	return in;
}

std::ostream& operator<< (std::ostream& out, const Human& human)
{
	out << human.Surname << std::endl << human.Name << std::endl << human.Patronymic << std::endl;
	return out;
}

Human& Human::set_Surname(const char* Surname)
{
	delete[] this->Surname;
	this->Surname = SetStr(Surname);
	return *this;
}
Human& Human::set_Name(const char* Name)
{
	delete[] this->Name;
	this->Name = SetStr(Name);
	return *this;
}
Human& Human::set_Patronymic(const char* Patronymic)
{
	delete[] this->Patronymic;
	this->Patronymic = SetStr(Patronymic);
	return *this;
}