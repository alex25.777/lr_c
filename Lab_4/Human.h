#pragma once
#include <iostream>

class Human
{
protected:
	char* Surname;
	char* Name;
	char* Patronymic;

public:
	Human();
	Human(const Human& human);
	~Human();

	Human& operator= (const Human& human);
	bool operator== (const Human& RightHuman);
	friend std::istream& operator>> (std::istream& in, Human& human);
	friend std::ostream& operator<< (std::ostream& out, const Human& human);

	Human& set_Surname(const char* Surname);
	char* const get_Surname() { return this->Surname; };

	Human& set_Name(const char* Name);
	const char* get_Name() { return this->Name; };

	Human& set_Patronymic(const char* Patronymic);
	const char* get_Patronymic() { return this->Patronymic; };

	virtual void Print_Sheet() {};
};
