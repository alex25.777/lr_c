#include <fstream>
#include <iomanip>
#include "Learner.h"
#include "service_functions.h"

int Learner::count = 0;

Learner::Learner()
{
	faculty = 0;
	course = 0;
	group = 0;
}

Learner::Learner(const Learner& learner)
{
	delete[] this->Surname;
	this->Surname = SetStr(learner.Surname);

	delete[] this->Name;
	this->Name = SetStr(learner.Name);

	delete[] this->Patronymic;
	this->Patronymic = SetStr(learner.Patronymic);

	this->faculty = learner.faculty;
	this->course = learner.course;
	this->group = learner.group;
}

Learner& Learner::operator=(const Learner& learner)
{
	if (this == &learner) return *this;

	delete[] this->Surname;
	this->Surname = SetStr(learner.Surname);

	delete[] this->Name;
	this->Name = SetStr(learner.Name);

	delete[] this->Patronymic;
	this->Patronymic = SetStr(learner.Patronymic);

	this->faculty = learner.faculty;
	this->course = learner.course;
	this->group = learner.group;

	return *this;
}

bool Learner::operator==(const Learner& RightLearner)
{
	bool result = (strcmp(this->Surname, RightLearner.Surname) == 0);
	result *= (strcmp(this->Name, RightLearner.Name) == 0);
	result *= (strcmp(this->Patronymic, RightLearner.Patronymic) == 0);
	result *= (this->faculty == RightLearner.faculty);
	result *= (this->course == RightLearner.course);
	result *= (this->group == RightLearner.group);
	return result;
}


std::istream& operator>>(std::istream& in, Learner& learner)
{
	delete[] learner.Surname;
	learner.Surname = SetStr(in);

	delete[] learner.Name;
	learner.Name = SetStr(in);

	delete[] learner.Patronymic;
	learner.Patronymic = SetStr(in);

	in >> learner.faculty;
	in >> learner.course;
	in >> learner.group;

	return in;
}

std::ostream& operator<<(std::ostream& out, const Learner& learner)
{
	out << learner.Surname << ' ';
	out << learner.Name << ' ';
	out << learner.Patronymic << ' ';
	out << learner.faculty << ' ';
	out << learner.course << ' ';
	out << learner.group << std::endl;
	return out;
}

Learner& Learner::input()
{
	std::cout << "������� ������� ���������:\n";
	delete[] this->Surname;
	this->Surname = SetStr();

	std::cout << "������� ��� ��������:\n";
	delete[] this->Name;
	this->Name = SetStr();

	std::cout << "������� ������� ��������:\n";
	delete[] this->Patronymic;
	this->Patronymic = SetStr();

	std::cout << "������� �����, ��������������� ��� ����������:\n";
	std::cout << "1-���, 2-��, 3-��.\n";
	ControlInputDigits(this->faculty, 1, 3);

	std::cout << "������� ����� ��� �����:\n";
	ControlInputDigits(this->course, 1, 4);

	std::cout << "������� ����� ��� ������:\n";
	ControlInputDigits(this->group, 1, 9);

	return *this;
}

Learner& Learner::set_faculty(int faculty)
{
	if (faculty >= 1 && faculty <= 3)
	{
		this->faculty = faculty;
	}
	return *this;
};
int Learner::get_faculty() { return this->faculty; };

Learner& Learner::set_course(int course)
{
	if (course >= 1 && course <= 3)
	{
		this->course = course;
	}
	return *this;
};
int Learner::get_course() { return this->course; };

Learner& Learner::set_group(int group)
{
	if (group >= 1 && group <= 3)
	{
		this->group = group;
	}
	return *this;
};
int Learner::get_group() { return this->group; };


char* Learner::FCG()
{
	char string_faculty[4][4] = { " ", "���", "��", "��" };
	int len = strlen(string_faculty[this->faculty]);
	char* string_group = new char[len + 4];
	strcpy(string_group, string_faculty[this->faculty]);
	string_group[len] = '-';
	string_group[len + 1] = char(this->course + 48);
	string_group[len + 2] = char(this->group + 48);
	string_group[len + 3] = '\0';
	return string_group;
}

void Learner::Add_Learner(Learner*& p_learner)
{
	int size = Learner::get_count();

	Learner* p = NULL;
	if ((p = new Learner[size + 1]) == NULL)
	{
		printf("������: ������ �� ��������.");
		return;
	}
	else
	{
		for (int i = 0; i < size; i++)
		{
			p[i] = p_learner[i];
		}
		p[size].input();
		delete[] p_learner;
		p_learner = p;
		set_count(size + 1);
	}
	std::cout << "\n\n������� ��������.\n\n\n";
}

Learner* Learner::Choose_Learner(Learner* p_learner)
{
	int size = Learner::get_count();
	if (size == 0)
	{
		std::cout << "��� ������ � ���������.\n\n\n";
		return NULL;
	}

	std::cout << "�������� ������� �������� � �������� ��������������� �����.\n";
	for (int i = 0; i < size; i++)
	{
		std::cout << std::right << std::setw(3) << i + 1 << ") ���: " << p_learner[i].Surname << " " << p_learner[i].Name << " " << p_learner[i].Patronymic << std::endl;
		char* string_group = p_learner[i].FCG();
		std::cout << "     ������: " << string_group << "\n\n";
		delete[] string_group;
	}

	int position;
	ControlInputDigits(position, 1, size);
	return &(p_learner[position - 1]);
}

void Learner::Print_Sheet()
{
	if (this == NULL) return;

	int length_first_column = 20, maxlength = 70;

	title(maxlength, "���������� � ��������");
	text(maxlength, length_first_column, "�������", this->Surname);
	text(maxlength, length_first_column, "���", this->Name);
	text(maxlength, length_first_column, "��������", this->Patronymic);
	char* string_group = this->FCG();
	text(maxlength, length_first_column, "������", string_group);
	delete[] string_group;
	std::cout << "\n\n\n";
}

void Learner::Save_In_File(const char* fname, Learner* p_learner)
{
	int size = get_count();
	if (size == 0)
	{
		std::cout << "������: ���� ������ �� ���������.";
		return;
	}

	std::ofstream file_out;
	file_out.open(fname, std::ios::binary);

	if (!file_out.is_open())
	{
		std::cout << "������: ���� �� ������.";
		return;
	}

	file_out << size << std::endl;

	for (int i = 0; i < size; i++)
	{
		file_out << p_learner[i];
	}

	file_out.close();
	std::cout << "���������� �������.\n\n\n";
}

void Learner::Loading_From_File(const char* fname, Learner*& p_learner)
{
	if ((get_count() == 0) && (p_learner != NULL))
	{
		p_learner = NULL;
	}

	std::ifstream file_in;
	file_in.open(fname, std::ios::binary);

	if (!file_in.is_open())
	{
		std::cout << "������: ���� �� ������.";
		return;
	}

	int size = 0;
	file_in >> size;
	file_in.seekg(1, std::ios_base::cur);
	if (file_in.eof()) return;

	Learner* p = NULL;
	if ((p = new Learner[size]) == NULL)
	{
		printf("������: ������ �� ��������.");
		file_in.close();
		return;
	}
	else
	{
		char buff[255];
		for (int i = 0; i < size; i++)
		{
			file_in.getline(buff, 255, ' ');
			p[i].set_Surname(buff);

			file_in.getline(buff, 255, ' ');
			p[i].set_Name(buff);

			file_in.getline(buff, 255, ' ');
			p[i].set_Patronymic(buff);

			file_in.getline(buff, 255, ' ');
			p[i].set_faculty(atoi(buff));
			
			file_in.getline(buff, 255, ' ');
			p[i].set_course(atoi(buff));

			file_in.getline(buff, 255, '\n');
			p[i].set_group(atoi(buff));
		}
		delete[] p_learner;
		p_learner = p;
		set_count(size);
		file_in.close();
	}
}

void Learner::FIO_Scan(Learner* p_learner, int size_input, Learner*& output, int& size_output)
{
	if (p_learner == NULL)
	{
		std::cout << "��� ������ � ���������.\n\n\n";
		size_output = 0;
		output = NULL;
		return;
	}

	std::cout << "������� ������� ��������:\n";
	char* Surname = SetStr();
	std::cout << "������� ��� ��������:\n";
	char* Name = SetStr();
	std::cout << "������� ������� ��������:\n";
	char* Patronymic = SetStr();

	bool scan_sucseful = false, bs, bn, bp;

	int* index = new int[size_input];
	int k = 0;

	for (int i = 0; i < size_input; i++)
	{
		bs = strcmp(Surname, p_learner[i].Surname) == 0;
		bn = strcmp(Name, p_learner[i].Name) == 0;
		bp = strcmp(Patronymic, p_learner[i].Patronymic) == 0;

		if (bs && bn && bp)
		{
			scan_sucseful = true;
			index[k] = i;
			k++;
		}
	}
	delete[]Surname;
	delete[]Name;
	delete[]Patronymic;

	if (!scan_sucseful)
	{
		size_output = 0;
		output = NULL;
	}
	else
	{
		size_output = k;
		output = new Learner[k];

		for (int i = 0; i < k; i++)
		{
			output[i] = p_learner[index[i]];
		}

	}
	delete[] index;
}

void Learner::Faculty_Scan(Learner* p_learner, int size_input, Learner*& output, int& size_output)
{
	if (p_learner == NULL)
	{
		std::cout << "��� ������ � ���������.\n\n\n";
		size_output = 0;
		output = NULL;
		return;
	}

	std::cout << "������� �����, ��������������� ��� ����������:\n";
	std::cout << "1-���, 2-��, 3-��.\n";
	int faculty;
	ControlInputDigits(faculty, 1, 3);

	bool scan_sucseful = false;

	int* index = new int[size_input];
	int k = 0;

	for (int i = 0; i < size_input; i++)
	{
		if ((faculty == p_learner[i].faculty))
		{
			scan_sucseful = true;
			index[k] = i;
			k++;
		}
	}

	if (!scan_sucseful)
	{
		size_output = 0;
		output = NULL;
	}
	else
	{
		size_output = k;
		output = new Learner[k];

		for (int i = 0; i < k; i++)
		{
			output[i] = p_learner[index[i]];
		}

	}
	delete[] index;
}

void Learner::Course_Scan(Learner* p_learner, int size_input, Learner*& output, int& size_output)
{
	if (p_learner == NULL)
	{
		std::cout << "��� ������ � ���������.\n\n\n";
		size_output = 0;
		output = NULL;
		return;
	}

	std::cout << "������� ����� ��� �����:\n";
	int course;
	ControlInputDigits(course, 1, 4);

	bool scan_sucseful = false;

	int* index = new int[size_input];
	int k = 0;

	for (int i = 0; i < size_input; i++)
	{
		if (course == p_learner[i].course)
		{
			scan_sucseful = true;
			index[k] = i;
			k++;
		}
	}

	if (!scan_sucseful)
	{
		size_output = 0;
		output = NULL;
	}
	else
	{
		size_output = k;
		output = new Learner[k];

		for (int i = 0; i < k; i++)
		{
			output[i] = p_learner[index[i]];
		}

	}
	delete[] index;
}

void Learner::Group_Scan(Learner* p_learner, int size_input, Learner*& output, int& size_output)
{
	if (p_learner == NULL)
	{
		std::cout << "��� ������ � ���������.\n\n\n";
		size_output = 0;
		output = NULL;
		return;
	}

	std::cout << "������� �����, ��������������� ��� ����������:\n";
	std::cout << "1-���, 2-��, 3-��.\n";
	int faculty;
	ControlInputDigits(faculty, 1, 3);

	std::cout << "������� ����� ��� �����:\n";
	int course;
	ControlInputDigits(course, 1, 4);

	std::cout << "������� ����� ��� ������:\n";
	int group;
	ControlInputDigits(group, 1, 10);

	bool scan_sucseful = false, bf, bc, bg;

	int* index = new int[size_input];
	int k = 0;

	for (int i = 0; i < size_input; i++)
	{
		bf = (faculty == p_learner[i].faculty);
		bc = (course == p_learner[i].course);
		bg = (group == p_learner[i].group);

		if (bf && bc && bg)
		{
			scan_sucseful = true;
			index[k] = i;
			k++;
		}
	}

	if (!scan_sucseful)
	{
		size_output = 0;
		output = NULL;
	}
	else
	{
		size_output = k;
		output = new Learner[k];

		for (int i = 0; i < k; i++)
		{
			output[i] = p_learner[index[i]];
		}

	}
	delete[] index;


}

