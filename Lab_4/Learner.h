#pragma once
#include "Human.h"

class Learner : public Human
{
private:
	int faculty;
	int course;
	int group;

	static int count;

public:
	Learner();
	Learner(const Learner& learner);
	~Learner() {};

	Learner& operator= (const Learner& learner);
	bool operator== (const Learner& RightLearner);
	friend std::istream& operator>> (std::istream& in, Learner& learner);
	friend std::ostream& operator<< (std::ostream& out, const Learner& learner);

	Learner& input();

	Learner& set_faculty(int faculty);
	int get_faculty();

	Learner& set_course(int course);
	int get_course();

	Learner& set_group(int group);
	int get_group();

	static void set_count(int a) { count = a; }
	static int get_count() { return count; }

	char* FCG(); // ����� ������ �� ���������� ����� � ������ ������, �������� ���-12

	static void Add_Learner(Learner*& p_learner);
	static Learner* Choose_Learner(Learner* p_learner);
	void Print_Sheet() override;
	static void Save_In_File(const char* fname, Learner* p_learner);
	static void Loading_From_File(const char* fname, Learner*& p_learner);

	static void FIO_Scan(Learner* p_learner, int size_input, Learner*& output, int& size_output);
	static void Faculty_Scan(Learner* p_learner, int size_input, Learner*& output, int& size_output);
	static void Course_Scan(Learner* p_learner, int size_input, Learner*& output, int& size_output);
	static void Group_Scan(Learner* p_learner, int size_input, Learner*& output, int& size_output);
};

