#include <fstream>
#include <iomanip>
#include "Teacher.h"
#include "service_functions.h"

int Teacher::count = 0;

Teacher::Teacher()
{
	this->department = 0;
	this->academic_rank = 0;
	this->subject = SetStr(" ");
}

Teacher::Teacher(const Teacher& teacher)
{
	delete[] this->Surname;
	this->Surname = SetStr(teacher.Surname);

	delete[] this->Name;
	this->Name = SetStr(teacher.Name);

	delete[] this->Patronymic;
	this->Patronymic = SetStr(teacher.Patronymic);

	this->department = teacher.department;
	this->academic_rank = teacher.academic_rank;

	delete[] this->subject;
	this->subject = SetStr(teacher.subject);
}

Teacher::~Teacher()
{
	delete[] this->subject;
}

bool Teacher::operator==(const Teacher& RightTeacher)
{
	bool result = (strcmp(this->Surname, RightTeacher.Surname) == 0);
	result *= (strcmp(this->Name, RightTeacher.Name) == 0);
	result *= (strcmp(this->Patronymic, RightTeacher.Patronymic) == 0);
	result *= (this->department == RightTeacher.department);
	result *= (this->academic_rank == RightTeacher.academic_rank);
	result *= (strcmp(this->subject, RightTeacher.subject) == 0);
	return result;
}

Teacher& Teacher::operator= (const Teacher& teacher)
{
	if (this == &teacher) return *this;

	delete[] this->Surname;
	this->Surname = SetStr(teacher.Surname);

	delete[] this->Name;
	this->Name = SetStr(teacher.Name);

	delete[] this->Patronymic;
	this->Patronymic = SetStr(teacher.Patronymic);

	this->department = teacher.department;
	this->academic_rank = teacher.academic_rank;

	delete[] this->subject;
	this->subject = SetStr(teacher.subject);

	return *this;
}

std::istream& operator>> (std::istream& in, Teacher& prepod)
{
	delete[] prepod.Surname;
	prepod.Surname = SetStr(in);

	delete[] prepod.Name;
	prepod.Name = SetStr(in);

	delete[] prepod.Patronymic;
	prepod.Patronymic = SetStr(in);

	in >> prepod.department;

	in >> prepod.academic_rank;

	delete[] prepod.subject;
	prepod.subject = SetStr(in);

	return in;
}

std::ostream& operator<< (std::ostream& out, const Teacher& teacher)
{
	out << teacher.Surname << ' ';
	out << teacher.Name << ' ';
	out << teacher.Patronymic << ' ';
	out << teacher.department << ' ';
	out << teacher.academic_rank << ' ';
	out << teacher.subject << std::endl;
	return out;
}

Teacher& Teacher::input()
{
	std::cout << "������� ������� �������������:\n";
	delete[] this->Surname;
	this->Surname = SetStr();

	std::cout << "������� ��� �������������:\n";
	delete[] this->Name;
	this->Name = SetStr();

	std::cout << "������� ������� �������������:\n";
	delete[] this->Patronymic;
	this->Patronymic = SetStr();

	std::cout << "������� �����, ��������������� ��� �������:\n";
	std::cout << "1-�����, 2-��, 3-��.\n";
	ControlInputDigits(this->department, 1, 3);

	std::cout << "������� �����, ��������������� ��� ������� ������:\n";
	std::cout << "1-�������������, 2-������, 3-������ ����.\n";
	ControlInputDigits(this->academic_rank, 1, 3);

	std::cout << "������� �������, ������� �� ��������:\n";
	delete[] this->subject;
	this->subject = SetStr();

	return *this;
}

Teacher& Teacher::set_Department(int department)
{
	if (department >= 1 && department <= 3)
	{
		this->department = department;
	}
	return *this;
}
int Teacher::get_Department() { return this->department; }

Teacher& Teacher::set_Academic_Rank(int academic_rank)
{
	if (academic_rank >= 1 && academic_rank <= 3)
	{
		this->academic_rank = academic_rank;
	}
	return *this;
}
int Teacher::get_Academic_Rank() { return this->academic_rank; }

Teacher& Teacher::set_Subject(const char* subject)
{
	delete[] this->subject;
	this->subject = SetStr(subject);
	return *this;
}
const char* Teacher::get_Subject() { return this->subject; }

void Teacher::Add_Teacher(Teacher*& p_teacher)
{
	int size = Teacher::get_count();

	Teacher* p = NULL;
	if ((p = new Teacher[size + 1]) == NULL)
	{
		printf("������: ������ �� ��������.");
		return;
	}
	else
	{
		for (int i = 0; i < size; i++)
		{
			p[i] = p_teacher[i];
		}
		p[size].input();
		delete[] p_teacher;
		p_teacher = p;
		set_count(size + 1);
	}
	std::cout << "\n\n������������� ��������.\n\n\n";
}

Teacher* Teacher::Choose_Teacher(Teacher* p_teacher)
{
	int size = Teacher::get_count();
	if (size == 0)
	{
		std::cout << "��� ������ � ��������������.\n\n\n";
		return NULL;
	}

	std::cout << "�������� ������� ������������� � �������� ��������������� �����.\n";
	for (int i = 0; i < size; i++)
	{
		std::cout << std::right << std::setw(3) << i + 1 << ") ���: " << p_teacher[i].Surname << " " << p_teacher[i].Name << " " << p_teacher[i].Patronymic << std::endl;
		char string_departments[4][11] = { " ", "�����", "��", "��" };
		std::cout << "     �������: " << string_departments[p_teacher[i].department] << "\n\n";
	}

	int position;
	ControlInputDigits(position, 1, size);
	return &(p_teacher[position - 1]);
}

void Teacher::Print_Sheet()
{
	if (this == NULL) return;

	int length_first_column = 20, maxlength = 70;

	title(maxlength, "���������� � �������������");
	text(maxlength, length_first_column, "�������", this->Surname);
	text(maxlength, length_first_column, "���", this->Name);
	text(maxlength, length_first_column, "��������", this->Patronymic);

	char string_academic_rank[4][14] = { " ", "�������������", "������", "������� ����" };
	text(maxlength, length_first_column, "������ ������", string_academic_rank[this->academic_rank]);

	char string_departments[4][11] = { " ", "�����", "��", "��" };
	text(maxlength, length_first_column, "�������", string_departments[this->department]);

	text(maxlength, length_first_column, "�������", this->subject);
	std::cout << "\n\n\n";
}

void Teacher::Save_In_File(const char* fname, Teacher* p_teacher)
{
	int size = get_count();
	if (size == 0)
	{
		std::cout << "������: ���� ������ �� ���������.";
		return;
	}

	std::ofstream file_out;
	file_out.open(fname, std::ios::binary);

	if (!file_out.is_open())
	{
		std::cout << "������: ���� �� ������.";
		return;
	}

	file_out << size << std::endl;

	for (int i = 0; i < size; i++)
	{
		file_out << p_teacher[i];
	}

	file_out.close();
	std::cout << "���������� �������.\n\n\n";
}

void Teacher::Loading_From_File(const char* fname, Teacher*& p_teacher)
{
	if ((get_count() == 0) && (p_teacher != NULL))
	{
		p_teacher = NULL;
	}

	std::ifstream file_in;
	file_in.open(fname, std::ios::binary);

	if (!file_in.is_open())
	{
		std::cout << "������: ���� �� ������.";
		return;
	}

	int size = 0;
	file_in >> size;
	file_in.seekg(1, std::ios_base::cur);
	if (file_in.eof()) return;

	Teacher* p = NULL;
	if ((p = new Teacher[size]) == NULL)
	{
		printf("������: ������ �� ��������.");
		file_in.close();
		return;
	}
	else
	{
		char buff[255];
		for (int i = 0; i < size; i++)
		{
			file_in.getline(buff, 255, ' ');
			p[i].set_Surname(buff);

			file_in.getline(buff, 255, ' ');
			p[i].set_Name(buff);

			file_in.getline(buff, 255, ' ');
			p[i].set_Patronymic(buff);

			file_in.getline(buff, 255, ' ');
			p[i].set_Department(atoi(buff));
			//file_in.seekg(1, std::ios_base::cur);

			file_in.getline(buff, 255, ' ');
			p[i].set_Academic_Rank(atoi(buff));
			//file_in.seekg(1, std::ios_base::cur);

			file_in.getline(buff, 255, '\n');
			p[i].set_Subject(buff);
		}
		delete[] p_teacher;
		p_teacher = p;
		set_count(size);
		file_in.close();
	}
}


void Teacher::Subject_Scan(Teacher* p_teacher, int size_input, Teacher*& output, int& size_output)
{
	if (p_teacher == NULL)
	{
		std::cout << "��� ������ � ��������������.\n";
		size_output = 0;
		output = NULL;
		return;
	}

	std::cout << "������� �������:\n";
	char* subject = SetStr();
	bool scan_sucseful = false;

	int* index = new int[size_input];
	int k = 0;

	char departments[4][14] = { " ", "�����", "��", "��" };
	for (int i = 0; i < size_input; i++)
	{
		if (strcmp(subject, p_teacher[i].subject) == 0)
		{
			scan_sucseful = true;
			index[k] = i;
			k++;
		}
	}
	delete[] subject;

	if (!scan_sucseful)
	{
		size_output = 0;
		output = NULL;
	}
	else
	{
		size_output = k;
		output = new Teacher[k];

		for (int i = 0; i < k; i++)
		{
			output[i] = p_teacher[index[i]];
		}

	}
	delete[] index;
}

void Teacher::Department_Scan(Teacher* p_teacher, int size_input, Teacher*& output, int& size_output)
{
	if (p_teacher == NULL)
	{
		std::cout << "��� ������ � ��������������.\n";
		size_output = 0;
		output = NULL;
		return;
	}

	std::cout << "������� �����, ��������������� ����� �������:\n";
	std::cout << "1-�����, 2-��, 3-��.\n";

	int department;
	ControlInputDigits(department, 1, 3);

	int* index = new int[size_input];
	int k = 0;

	bool scan_sucseful = false;
	for (int i = 0; i < size_input; i++)
	{
		if (department == p_teacher[i].department)
		{
			scan_sucseful = true;
			index[k] = i;
			k++;
		}
	}

	if (!scan_sucseful)
	{
		size_output = 0;
		output = NULL;
	}
	else
	{
		size_output = k;
		output = new Teacher[k];

		for (int i = 0; i < k; i++)
		{
			output[i] = p_teacher[index[i]];
		}

	}
	delete[] index;

}

void Teacher::FIO_Scan(Teacher* p_teacher, int size_input, Teacher*& output, int& size_output)
{
	if (p_teacher == NULL)
	{
		std::cout << "��� ������ � ��������������.\n";
		size_output = 0;
		output = NULL;
		return;
	}

	std::cout << "������� ������� �������������:\n";
	char* Surname = SetStr();
	std::cout << "������� ��� �������������:\n";
	char* Name = SetStr();
	std::cout << "������� ������� �������������:\n";
	char* Patronymic = SetStr();

	bool scan_sucseful = false, bs, bn, bp;

	int* index = new int[size_input];
	int k = 0;

	char departments[4][14] = { " ", "�����", "��", "��" };
	for (int i = 0; i < size_input; i++)
	{
		bs = strcmp(Surname, p_teacher[i].Surname) == 0;
		bn = strcmp(Name, p_teacher[i].Name) == 0;
		bp = strcmp(Patronymic, p_teacher[i].Patronymic) == 0;

		if (bs && bn && bp)
		{
			scan_sucseful = true;
			index[k] = i;
			k++;
		}
	}
	delete[]Surname;
	delete[]Name;
	delete[]Patronymic;

	if (!scan_sucseful)
	{
		size_output = 0;
		output = NULL;
	}
	else
	{
		size_output = k;
		output = new Teacher[k];

		for (int i = 0; i < k; i++)
		{
			output[i] = p_teacher[index[i]];
		}

	}
	delete[] index;
}
