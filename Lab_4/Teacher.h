#pragma once
#include "Human.h"
#include "service_functions.h"

class Teacher : public Human
{
private:
	int department;
	int academic_rank;
	char* subject;

	static int count;

public:
	Teacher();
	Teacher(const Teacher& teacher);
	~Teacher();


	Teacher& operator= (const Teacher& teacher);
	bool operator== (const Teacher& RightTeacher);
	friend std::istream& operator>> (std::istream& in, Teacher& teacher);
	friend std::ostream& operator<< (std::ostream& out, const Teacher& teacher);

	Teacher& input();

	Teacher& set_Department(int department);
	int get_Department();

	Teacher& set_Academic_Rank(int academic_rank);
	int get_Academic_Rank();

	Teacher& set_Subject(const char* subject);
	const char* get_Subject();

	static void set_count(int a) { count = a; }
	static int get_count() { return count; }

	static void Add_Teacher(Teacher*& p_teacher);
	static Teacher* Choose_Teacher(Teacher* p_teacher);
	void Print_Sheet() override;
	static void Save_In_File(const char* fname, Teacher* p_teacher);
	static void Loading_From_File(const char* fname, Teacher*& p_teacher);

	static void Subject_Scan(Teacher* p_teacher, int size_input, Teacher*& output, int& size_output);
	static void Department_Scan(Teacher* p_teacher, int size_input, Teacher*& output, int& size_output);
	static void FIO_Scan(Teacher* p_teacher, int size_input, Teacher*& output, int& size_output);
};
