#include <windows.h>
#include <fstream>
#include <clocale>
#include "Teacher.h"
#include "Learner.h"
#include "service_functions.h"
#include <iomanip>

enum Command { NO_INFO, TEACHER, LEARNER, EXIT, CONTINUE };

Command Menu_DB();
int Menu_Teacher();
int Menu_Learner();

int main()
{
	setlocale(LC_ALL, "Russian");

	Teacher* DB_Techer = NULL;
	Learner* DB_Learner = NULL;

	Command command = CONTINUE, choose_DB = NO_INFO;

	while (true)
	{
		if (choose_DB == NO_INFO)
		{
			switch (choose_DB = Menu_DB())
			{
			case TEACHER:
			{
				Teacher::Loading_From_File("DB_Techer", DB_Techer);
				command = CONTINUE;
				break;
			}

			case LEARNER:
			{
				Learner::Loading_From_File("DB_Learner", DB_Learner);
				command = CONTINUE;
				break;
			}

			case EXIT:
			{
				exit(1);
			}

			}
		}

		// ������� � ���������������
		while (choose_DB == TEACHER)
		{
			switch (Menu_Teacher())
			{

			case 1:
				Teacher::Add_Teacher(DB_Techer);
				break;

			case 2:
				Teacher::Save_In_File("DB_Techer", DB_Techer);
				break;

			case 3:
				{
				Teacher * teacher = Teacher::Choose_Teacher(DB_Techer);
				if (teacher != NULL) teacher->Print_Sheet();
				break;
				}

			case 4:
			{
				int size_input = Teacher::get_count();
				Teacher* output = NULL;
				int size_output = 0;
				Teacher::Subject_Scan(DB_Techer, size_input, output, size_output);

				if (output == NULL)
				{
					std::cout << "������ �� �������.\n\n\n";
					break;
				}

				std::cout << "������������� �� ����� ��������:\n";
				for (int i = 0; i < size_output; i++)
				{
					std::cout << std::right << std::setw(3) << i + 1 << ") ���: " << output[i].get_Surname() << " " << output[i].get_Name() << " " << output[i].get_Patronymic() << std::endl;
					char string_departments[4][6] = { " ", "�����", "��", "��" };
					std::cout << "     �������: " << string_departments[output[i].get_Department()] << "\n\n";
				}
				std::cout << "\n";
				delete[] output;

				break;
			}

			case 5:
			{
				int size_input = Teacher::get_count();
				Teacher* output = NULL;
				int size_output = 0;
				Teacher::Department_Scan(DB_Techer, size_input, output, size_output);

				if (output == NULL)
				{
					std::cout << "������ �� �������.\n\n\n";
					break;
				}

				std::cout << "������������� ���� �������:\n";
				for (int i = 0; i < size_output; i++)
				{
					std::cout << std::right << std::setw(3) << i + 1 << ") ���: " << output[i].get_Surname() << " " << output[i].get_Name() << " " << output[i].get_Patronymic() << std::endl;
				}
				std::cout << "\n\n";
				delete[] output;
				break;
			}

			case 6:
			{
				int size_input = Teacher::get_count();
				Teacher* output = NULL;
				int size_output = 0;
				Teacher::FIO_Scan(DB_Techer, size_input, output, size_output);

				if (output == NULL)
				{
					std::cout << "������ �� �������.\n\n\n";
					break;
				}

				std::cout << "������������� � ������ ���:\n";
				for (int i = 0; i < size_output; i++)
				{
					std::cout << std::right << std::setw(3) << i + 1 << ") ���: " << output[i].get_Surname() << " " << output[i].get_Name() << " " << output[i].get_Patronymic() << std::endl;
					char string_departments[4][6] = { " ", "�����", "��", "��" };
					std::cout << "     �������: " << string_departments[output[i].get_Department()] << "\n\n";
				}
				std::cout << "\n";
				delete[] output;
				break;
			}

			case 7:
			{
				delete[] DB_Techer;
				DB_Techer = NULL;
				Teacher::set_count(0);
				choose_DB = NO_INFO;
				break;
			}
			}
		}

		// ������ �� ����������
		while (choose_DB == LEARNER)
		{
			switch (Menu_Learner())
			{
			case 1:
				Learner::Add_Learner(DB_Learner);
				break;

			case 2:
				Learner::Save_In_File("DB_Learner", DB_Learner);
				break;

			case 3:
			{
				Learner * learner = Learner::Choose_Learner(DB_Learner);
				if(learner != NULL) learner->Print_Sheet();
				break;
			}

			case 4:
			{
				int size_input = Learner::get_count();
				Learner* output = NULL;
				int size_output = 0;
				Learner::FIO_Scan(DB_Learner, size_input, output, size_output);

				if (output == NULL)
				{
					std::cout << "������ �� �������.\n\n\n";
					break;
				}

				std::cout << "�������� � ������ ���:\n";
				for (int i = 0; i < size_output; i++)
				{
					std::cout << std::right << std::setw(3) << i + 1 << ") ���: " << output[i].get_Surname() << " " << output[i].get_Name() << " " << output[i].get_Patronymic() << std::endl;
					char* string_group = output[i].FCG();
					std::cout << "     ������: " << string_group << "\n\n";
					delete[] string_group;
				}
				std::cout << "\n";
				delete[] output;
				break;
			}

			case 5:
			{
				int size_input = Learner::get_count();
				Learner* output = NULL;
				int size_output = 0;
				Learner::Faculty_Scan(DB_Learner, size_input, output, size_output);

				if (output == NULL)
				{
					std::cout << "������ �� �������.\n\n\n";
					break;
				}

				std::cout << "�������� ����� ����������:\n";
				for (int i = 0; i < size_output; i++)
				{
					std::cout << std::right << std::setw(3) << i + 1 << ") ���: " << output[i].get_Surname() << " " << output[i].get_Name() << " " << output[i].get_Patronymic() << std::endl;
					char* string_group = output[i].FCG();
					std::cout << "     ������: " << string_group << "\n\n";
					delete[] string_group;
				}
				std::cout << "\n";
				delete[] output;
				break;
			}

			case 6:
			{
				int size_input = Learner::get_count();
				Learner* output = NULL;
				int size_output = 0;
				Learner::Course_Scan(DB_Learner, size_input, output, size_output);

				if (output == NULL)
				{
					std::cout << "������ �� �������.\n\n\n";
					break;
				}

				std::cout << "�������� ����� �����:\n";
				for (int i = 0; i < size_output; i++)
				{
					std::cout << std::right << std::setw(3) << i + 1 << ") ���: " << output[i].get_Surname() << " " << output[i].get_Name() << " " << output[i].get_Patronymic() << std::endl;
					char* string_group = output[i].FCG();
					std::cout << "     ������: " << string_group << "\n\n";
					delete[] string_group;
				}
				std::cout << "\n";
				delete[] output;
				break;
			}

			case 7:
			{
				int size_input = Learner::get_count();
				Learner* output = NULL;
				int size_output = 0;
				Learner::Group_Scan(DB_Learner, size_input, output, size_output);

				if (output == NULL)
				{
					std::cout << "\n������ �� �������.\n\n\n";
					break;
				}

				std::cout << "�������� ���� ������:\n";
				for (int i = 0; i < size_output; i++)
				{
					std::cout << std::right << std::setw(3) << i + 1 << ") ���: " << output[i].get_Surname() << " " << output[i].get_Name() << " " << output[i].get_Patronymic() << std::endl;
				}
				std::cout << "\n\n";
				delete[] output;
				break;


			}

			case 8:
			{
				delete[] DB_Learner;
				DB_Learner = NULL;
				Learner::set_count(0);
				choose_DB = NO_INFO;
				break;
			}
			}
		}
	}
	system("pause");
	return 0;
}

Command Menu_DB()
{
	std::cout << "����: ����� ���� ���� ������.\n";
	std::cout << "������� ����� ������ �������:\n";
	std::cout << "1) �������� � ���������������.\n";
	std::cout << "2) �������� �� ����������.\n";
	std::cout << "3) �����.\n";

	int com;
	ControlInputDigits(com, 1, 3);
	std::cout << "\n\n";

	switch (com)
	{
	case 1: return TEACHER;
	case 2: return LEARNER;
	case 3: return EXIT;
	}
}

int Menu_Teacher()
{
	std::cout << "����: �������������.\n";
	std::cout << "������� ����� ������ �������:\n";
	std::cout << "1) �������� �������������.\n";
	std::cout << "2) ���������.\n";
	std::cout << "3) ������� ������� �� ���������� � �������������.\n";
	std::cout << "4) ����� ������������� �� ��������� ��������.\n";
	std::cout << "5) ����� �������������� �� �������� �������.\n";
	std::cout << "6) ����� �������������� �� ���.\n";
	std::cout << "7) �������� � ���� ������ ���� ������.\n";

	int com;
	ControlInputDigits(com, 1, 7);
	std::cout << "\n\n";

	return com;
}

int Menu_Learner()
{
	std::cout << "����: ��������.\n";
	std::cout << "������� ����� ������ �������:\n";
	std::cout << "1) �������� ��������.\n";
	std::cout << "2) ���������.\n";
	std::cout << "3) ������� ������� �� ���������� � ��������.\n";
	std::cout << "4) ����� ��������� �� ���.\n";
	std::cout << "5) ����� ��������� �� ����������.\n";
	std::cout << "6) ����� ��������� �� �����.\n";
	std::cout << "7) ����� ��������� �� ������.\n";
	std::cout << "8) �������� � ���� ������ ���� ������.\n";

	int com;
	ControlInputDigits(com, 1, 8);
	std::cout << "\n\n";

	return com;
}

