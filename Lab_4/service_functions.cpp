#include <iostream>
#include <fstream>
#include "service_functions.h"

char* SetStr()
{
	char buff[255];
	ControlInputString(buff, 255);

	char* str = new char[strlen(buff) + 1];
	if (str != NULL) strcpy(str, buff);

	return str;
}

char* SetStr(std::istream& in)
{
	char buff[255];
	in >> buff;

	char* str = new char[strlen(buff) + 1];
	if (str != NULL) strcpy(str, buff);

	return str;
}

char* SetStr(const char* buff)
{
	char* str = new char[strlen(buff) + 1];
	if (str != NULL) strcpy(str, buff);

	return str;
}

void ControlInputString(char* buff, int size_buff)
{
	do
	{
		std::cin.get(buff, size_buff);
		std::cin.clear();
		std::cin.ignore(999, '\n');
	} while (!std::cin.good() || buff[0] == '\0');
}

void ControlInputDigits(int& digit, int down_border, int up_border)
{
	bool input_successful;
	do
	{
		std::cin >> digit;
		if (std::cin.fail() || std::cin.peek() != '\n')
		{
			input_successful = false;
			std::cout << "����� ������ �����.\n";
			std::cin.clear();
			std::cin.ignore(999, '\n');
		}
		else
		{
			input_successful = true;
			if (down_border <= up_border) // �������� ����������� ���� ������. �� ����������� ��� ���������� �� ���������.
			{
				if (digit < down_border || digit > up_border)
				{
					std::cout << "����� ������ ����� �� " << down_border << " �� " << up_border << ".\n";
					input_successful = false;
				}
			}
		}
	} while (!input_successful);

	std::cin.clear();
	std::cin.ignore(999, '\n');
}

void line(int length, char ch)
{
	for (int i = 0; i < length; i++) std::cout << ch;
}

void line(int maxlength, int length_first_column, char ch)
{
	maxlength -= 3;
	length_first_column -= 1;
	std::cout << "|";
	line(length_first_column, ch);
	std::cout << "|";
	line(maxlength - length_first_column, ch);
	std::cout << "|" << std::endl;
}

void text(int maxlength, int length_first_column, const char* string_first_column, const char* string_second_column)
{
	int length_string_first_column = strlen(string_first_column);
	int length_string_second_column = strlen(string_second_column);

	std::cout << "| " << string_first_column;
	int space_1 = length_first_column - (length_string_first_column + 2);
	line(space_1, ' ');

	std::cout << "| " << string_second_column;
	int space_2 = maxlength - (length_first_column + length_string_second_column + 3);
	line(space_2, ' ');
	std::cout << "|" << std::endl;

	line(maxlength, length_first_column, '_');
}

void title(int maxlength, const char* title)
{
	line(maxlength, '_');
	std::cout << std::endl;

	int length_title = strlen(title);
	int left_space = (maxlength - length_title - 2) / 2;
	std::cout << "|";
	for (int i = 0; i < left_space; i++) std::cout << " ";
	std::cout << title;
	int right_space = (maxlength - left_space - length_title - 2);
	for (int i = 0; i < right_space; i++) std::cout << " ";
	std::cout << "|" << std::endl;

	std::cout << '|';
	line(maxlength - 2, '_');
	std::cout << '|' << std::endl;
}