#pragma once
#include <windows.h>
#include <iostream>

char* SetStr(); // ���� ������ � ���������� � ���������� ������
char* SetStr(std::istream& in); // ���� ������ �� �����
char* SetStr(const char* buff); // ����������� � ���������� ������

// �������� �����
void ControlInputString(char* buff, int size_buff = 255); // ������������ ����� �������� ������
void ControlInputDigits(int& digit, int down_border = 1, int up_border = -1); // ������������ ���� �����

// ������� ��� ������ �������
void line(int length, char ch);
void line(int maxlength, int length_first_column, char ch);

void text(int maxlength, int length_first_column, const char* string_first_column, const char* string_second_column);
void title(int maxlength, const char* title);
